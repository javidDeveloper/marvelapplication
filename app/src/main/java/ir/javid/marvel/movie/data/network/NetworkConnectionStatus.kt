package ir.javid.marvel.movie.data.network

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkRequest
import androidx.lifecycle.LiveData

/**
 * @author  : Javid
 * @since   : 3/1/2022 -- 18:56
 * @summary : ---
 */

class NetworkConnectionStatus(private val connectivityManager: ConnectivityManager) :
    LiveData<Boolean>() {


    constructor(
        application: Application,
    ) : this(application.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)

    private val networkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            postValue(true)
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            postValue(false)
        }
    }

    override fun onActive() {
        super.onActive()
        NetworkRequest.Builder().build().also {
            connectivityManager.registerNetworkCallback(it, networkCallback)
        }
    }

    override fun onInactive() {
        super.onInactive()
        connectivityManager.unregisterNetworkCallback(networkCallback)
    }

}

