package ir.javid.marvel.movie.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import ir.javid.marvel.movie.R
import ir.javid.marvel.movie.databinding.FragmentMovieListBinding

@AndroidEntryPoint
class MovieListFragment : Fragment() {
    private lateinit var mBinding: FragmentMovieListBinding
    private val vm: MovieListVM by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm.request()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        mBinding = FragmentMovieListBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.liveData.observe(viewLifecycleOwner) {
            mBinding.test.text = it.toString()
        }
    }

}