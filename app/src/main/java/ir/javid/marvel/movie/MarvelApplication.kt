package ir.javid.marvel.movie

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author  : Javid
 * @since   : 1/21/2022 -- 12:48
 * @summary : ---
 */

@HiltAndroidApp
class MarvelApplication : Application() {

}