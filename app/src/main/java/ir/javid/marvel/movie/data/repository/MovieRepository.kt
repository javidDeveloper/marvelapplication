package ir.javid.marvel.movie.data.repository

import ir.javid.marvel.movie.data.network.entity.movie.MovieEntity
import ir.javid.marvel.movie.data.utils.DataState
import kotlinx.coroutines.flow.Flow

/**
 * @author  : Javid
 * @since   : 1/25/2022 -- 18:12
 * @summary : ---
 */

interface MovieRepository {

    fun requestMovieBySearch(
        page: String,
        query: String,
        includeAdult: String
    ): Flow<DataState<MovieEntity>>
}