package ir.javid.marvel.movie.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ir.javid.marvel.movie.R
import ir.javid.marvel.movie.data.network.NetworkConnectionStatus
import ir.javid.marvel.movie.databinding.ActivityMainBinding

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var appBarConfiguration: AppBarConfiguration
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        showActionBar(true)
        initNavControl()
        initNetworkState()
    }

    private fun initNetworkState() {
        NetworkConnectionStatus(application).observe(this) { isNetworkConnect ->
            when (isNetworkConnect) {
                true -> {
                    Snackbar.make(binding.root, "Network is access", Snackbar.LENGTH_SHORT).show()
                }
                else -> {
                    Snackbar.make(binding.root, "Network is lose!!", Snackbar.LENGTH_SHORT).show()
                }
            }

        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment_container_main)
        return navController.navigateUp(appBarConfiguration)
                || super.onSupportNavigateUp()
    }


    private fun initNavControl() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_container_main) as NavHostFragment
        val navController: NavController = navHostFragment.navController
//        val appBarConfig = AppBarConfiguration(navController.graph)
        setupActionBarWithNavController(navController)
//        binding.toolbar.setNavigationOnClickListener {
//            navController.navigateUp(appBarConfig)
//        }
    }


    private fun showActionBar(isShow: Boolean) =
        when (isShow) {
            true -> supportActionBar?.show()
            false -> supportActionBar?.hide()
        }
}