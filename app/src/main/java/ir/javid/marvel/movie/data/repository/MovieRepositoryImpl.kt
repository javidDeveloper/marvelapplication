package ir.javid.marvel.movie.data.repository

import ir.javid.marvel.movie.data.network.NetworkDataSource
import ir.javid.marvel.movie.data.network.entity.movie.MovieEntity
import ir.javid.marvel.movie.data.network.entity.restError.RestError
import ir.javid.marvel.movie.data.utils.DataState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.collect
import org.json.JSONException
import java.lang.Exception

/**
 * @author  : Javid
 * @since   : 2/6/2022 -- 16:51
 * @summary : ---
 */

@ExperimentalCoroutinesApi
class MovieRepositoryImpl(
    private val networkDataSource: NetworkDataSource,
) : MovieRepository {


    override fun requestMovieBySearch(
        page: String,
        query: String,
        includeAdult: String,
    ): Flow<DataState<MovieEntity>> =
        callbackFlow {
            trySend(DataState.Loading)
            try {
                networkDataSource.requestMovieBySearch(
                    page = page,
                    query = query,
                    includeAdult = includeAdult,
                ).collect {
                    when (it) {
                        is DataState.ApiRestError -> {
                            trySend(DataState.ApiRestError(it.data))
                        }
                        is DataState.ApiSuccess -> {
                            trySend(DataState.ApiSuccess(it.data.toEntity()))
                        }
                        DataState.NetworkError -> {
                            trySend(DataState.NetworkError)
                        }
                    }
                }
            } catch (e: JSONException) {
                trySend(DataState.ApiRestError(
                    RestError(
                        code = -1,
                        message = e.message
                    )))
            } catch (e: Exception) {
                println(e.message)
                trySend(DataState.NetworkError)
            }
            awaitClose { cancel() }
        }
}