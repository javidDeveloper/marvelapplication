package ir.javid.marvel.movie.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import ir.javid.marvel.movie.data.network.NetworkDataSource
import ir.javid.marvel.movie.data.repository.MovieRepository
import ir.javid.marvel.movie.data.repository.MovieRepositoryImpl
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton

/**
 * @author  : Javid
 * @since   : 2/6/2022 -- 18:15
 * @summary : ---
 */

@ExperimentalCoroutinesApi
@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {


    @Singleton
    @Provides
    fun provideMovieRepository(
        networkDataSource: NetworkDataSource,
    ): MovieRepository = MovieRepositoryImpl(
        networkDataSource
    )

}