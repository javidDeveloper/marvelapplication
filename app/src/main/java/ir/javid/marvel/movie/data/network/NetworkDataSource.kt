package ir.javid.marvel.movie.data.network

import ir.javid.marvel.movie.data.network.authentication.Authentication
import ir.javid.marvel.movie.data.network.dto.movie.MovieDto
import ir.javid.marvel.movie.data.utils.DataState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import javax.inject.Inject

/**
 * @author  : Javid
 * @since   : 2/6/2022 -- 16:54
 * @summary : ---
 */

@ExperimentalCoroutinesApi
class  NetworkDataSource @Inject constructor(
    private val apiService: ApiService,
) {
    suspend fun requestMovieBySearch(
        page: String,
        includeAdult: String,
        query: String,
    ): Flow<DataState<MovieDto>> =
        callbackFlow {
            trySend(DataState.ApiSuccess(
                apiService.requestMovieBySearch(
                    apiKey = Authentication.apiKey,
                    language = "en-US",
                    page = page,
                    query = query,
                    includeAdult = includeAdult
                ))
            )
            awaitClose { close() }
        }
}