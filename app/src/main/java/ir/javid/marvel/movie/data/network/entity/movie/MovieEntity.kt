package ir.javid.marvel.movie.data.network.entity.movie

import ir.javid.marvel.movie.data.network.entity.Entity

/**
 * @author  : Javid
 * @since   : 1/25/2022 -- 17:42
 * @summary : ---
 */

data class MovieEntity(
    var page: Int,
    var results: List<ResultEntity>,
    var totalPages: Int,
    var totalResults: Int
) : Entity

data class ResultEntity(
    var adult: Boolean,
    var backdropPath: String,
    var genreIds: List<Int>,
    var id: Int,
    var originalLanguage: String,
    var originalTitle: String,
    var overview: String,
    var popularity: Double,
    var posterPath: String,
    var releaseDate: String,
    var title: String,
    var video: Boolean,
    var voteAverage: Double,
    var voteCount: Int
) : Entity

