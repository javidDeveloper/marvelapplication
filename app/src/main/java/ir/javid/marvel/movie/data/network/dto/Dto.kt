package ir.javid.marvel.movie.data.network.dto

import ir.javid.marvel.movie.data.network.entity.Entity

/**
 * @author  : Javid
 * @since   : 1/25/2022 -- 17:36
 * @summary : ---
 */

interface Dto {
    fun toEntity(): Entity
}