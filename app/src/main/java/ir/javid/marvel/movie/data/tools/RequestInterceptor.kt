package ir.javid.marvel.movie.data.tools

import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * @author  : Javid
 * @since   : 1/21/2022 -- 13:05
 * @summary : ---
 */

class RequestInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response =
        chain.proceed(
            chain.request().apply {
                newBuilder().method(method, body)
            }
        )
}