package ir.javid.marvel.movie.data.utils

import ir.javid.marvel.movie.data.network.entity.restError.RestError

/**
 * @author  : Javid
 * @since   : 1/25/2022 -- 18:41
 * @summary : ---
 */

sealed class DataState<out RES> {
    object Loading : DataState<Nothing>()
    object NetworkError : DataState<Nothing>()
    data class ApiSuccess<out RES>(val data : RES): DataState<RES>()
    data class ApiRestError(val data : RestError): DataState<Nothing>()
}