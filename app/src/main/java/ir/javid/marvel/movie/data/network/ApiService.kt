package ir.javid.marvel.movie.data.network

import ir.javid.marvel.movie.data.network.dto.movie.MovieDto
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * @author  : Javid
 * @since   : 1/25/2022 -- 17:21
 * @summary : ---
 */

interface ApiService {
    @GET("search/movie")
    suspend fun requestMovieBySearch(
        @Query("api_key") apiKey: String,
        @Query("language") language: String,
        @Query("page") page: String,
        @Query("include_adult") includeAdult: String,
        @Query("query") query: String,
    ) : MovieDto
}