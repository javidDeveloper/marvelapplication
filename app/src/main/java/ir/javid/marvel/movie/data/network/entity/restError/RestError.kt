package ir.javid.marvel.movie.data.network.entity.restError

/**
 * @author  : Javid
 * @since   : 1/25/2022 -- 18:48
 * @summary : ---
 */

data class RestError(
    val code: Int,
    val message: String?,
)