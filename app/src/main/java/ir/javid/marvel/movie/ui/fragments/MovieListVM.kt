package ir.javid.marvel.movie.ui.fragments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ir.javid.marvel.movie.data.network.entity.movie.MovieEntity
import ir.javid.marvel.movie.data.repository.MovieRepository
import ir.javid.marvel.movie.data.utils.DataState
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author  : Javid
 * @since   : 3/1/2022 -- 19:47
 * @summary : ---
 */

@HiltViewModel
class MovieListVM @Inject constructor(
    private val movieRepository: MovieRepository,
) : ViewModel() {
    var liveData: MutableLiveData<MovieEntity> = MutableLiveData()

    fun request() {
        viewModelScope.launch {
            movieRepository.requestMovieBySearch(
                "1",
                "spider-man",
                "true").collectLatest {
                when (it) {
                    is DataState.ApiRestError -> {

                    }
                    is DataState.ApiSuccess -> {
                        it?.data?.let {
                            liveData.postValue(it)
                        }
                    }
                    DataState.Loading -> {

                    }
                    DataState.NetworkError -> {

                    }
                }
            }
        }
    }
}